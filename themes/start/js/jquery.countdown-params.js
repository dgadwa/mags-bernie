/*
jQuery('#clock').countdown('2016/11/01', function(event) {
               jQuery(this).html(event.strftime(''
                   + 'In <span>%m</span> months '
                 + '<span>%d</span> days '
                 + '<span>%H</span> hr '
                 + '<span>%M</span> min '
                 + '<span>%S</span> sec'));
             });
*/             
             
jQuery('div#clock').countdown('2016/11/01', {elapse: true})
	.on('update.countdown', function(event) {
		  
    if (event.elapsed) { // Either true or false
      // Counting up...
      
              jQuery(this).html(event.strftime(''
            + 'It\'s been <span>%m</span> months '
            + '<span>%d</span> days '
            + '<span>%H</span> hr '
            + '<span>%M</span> min '
			+ '<span>%S</span> sec'));
      
      
    } else {
      // Countdown...
      
              jQuery(this).html(event.strftime(''
            + 'In <span>%m</span> months '
            + '<span>%d</span> days '
            + '<span>%H</span> hr '
            + '<span>%M</span> min '
			+ '<span>%S</span> sec'));      
      
    }
  });             