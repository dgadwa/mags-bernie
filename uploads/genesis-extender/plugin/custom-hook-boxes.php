<?php
/**
 * Build and Hook-In Custom Hook Boxes.
 */

/* Name: FAKE */

add_action( 'genesis_doctype', 'genesis_extender_fake_hook_box', 10 );
function genesis_extender_fake_hook_box() {
	genesis_extender_fake_hook_box_content();
}

function genesis_extender_fake_hook_box_content() { ?>

<?php
}

/* Name: javascript */

add_action( 'genesis_footer', 'genesis_extender_javascript_hook_box', 10 );
function genesis_extender_javascript_hook_box() {
	genesis_extender_javascript_hook_box_content();
}

function genesis_extender_javascript_hook_box_content() { ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.countdown/2.1.0/jquery.countdown.min.js"></script>
<?php
}
